# Terninger

**Forskningsspørsmål:** Er terningene rettferdige?

![Terninger](https://upload.wikimedia.org/wikipedia/commons/e/e5/Dice_%28typical_role_playing_game_dice%29.jpg)


## Urettferdig terning

Dersom man ikke har like stor sannsynlighet for å få hvert enkelt tall på terningen, sier vi terningen er urettferdig.
Det kan være flere ulike årsaker til at en terning er urettferdig, for eksempel
- Skjult lodd i terningen
- Ujevn geometrisk form
- Slipte kanter

Man kan undersøke terningene for å finne ut om de er rettferdige. Vi tester heller dette empirisk med et eksperiment.
Dette betyr at vi må bruke terningene mange ganger og kontrollere om de oppfører som forventet.

## Gjennomføring av eksperiment

Vi velger tre terninger:
- Blå (fire sider)
- Oransje (seks sider)
- Grønn (åtte sider)

Terningene trilles 100 ganger:
- Resultater noteres i filen "eksperimentdata.csv".
- Vi lager figurer og tabeller
- Vi sammenligner med forventede verdier for hver terning.
- Vi gjennomfører hypotesetester basert på [Kjikvadratfordelingen](https://en.wikipedia.org/wiki/Chi-squared_distribution) og konkluderer.

All analyse gjennomføres i programmeringsspråket [Python](https://www.python.org/)